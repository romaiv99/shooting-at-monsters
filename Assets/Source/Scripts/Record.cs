using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Record
{
    public const string EnemyCountSaveKey = "EnemyRecord";

    public static int GetCurrentRecord => PlayerPrefs.GetInt(EnemyCountSaveKey);
    public static int TryUpdateRecord(int enemyCount)
    {
        int oldRecord = GetCurrentRecord;
        if (oldRecord < enemyCount)
        {
            PlayerPrefs.SetInt(EnemyCountSaveKey, enemyCount);
            return enemyCount;
        }
        return oldRecord;
    }
}
