using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.ConstrainedExecution;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

public class Weapon : MonoBehaviour
{
    [SerializeField] private Transform _shootPoint;
    [SerializeField] private float _shootDrawLineDistance;

    [SerializeField] private float _shootDuraction = 0.5f;
    [SerializeField] private float _reductionTimePerUpgradeLevel = 0.1f;

    [SerializeField] private ParticleSystem _fire;
    [SerializeField] private Pool _bulletPool;

    private bool _isFireAwaleble = true;
    private WaitForSeconds _duraction;

    private void Start()
    {
        ReloadSpeedUpgrade reloadSpeedUpgrade = new ReloadSpeedUpgrade();

        _duraction = new WaitForSeconds(_shootDuraction - reloadSpeedUpgrade.GetLevel * _reductionTimePerUpgradeLevel);
    }

    private void OnDrawGizmos()
    {
        Debug.DrawLine(_shootPoint.position, _shootPoint.position+ _shootPoint.forward*_shootDrawLineDistance,
            Color.red);
    }

    public void Fire()
    {
        if (_isFireAwaleble)
        {
            _fire.Play();
            PoolObject objectPool = _bulletPool.GetObject();
            objectPool.transform.position = _shootPoint.position;
            objectPool.transform.rotation = _shootPoint.rotation;
            objectPool.Activate();
            StartCoroutine(FireDuraction());
        }
    }

    private IEnumerator FireDuraction()
    {
        _isFireAwaleble = false;
        yield return _duraction;
        _isFireAwaleble = true;
    }
}
