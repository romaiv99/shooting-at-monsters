using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(PoolObject), typeof(Collider))]
public class Bullet : MonoBehaviour, IPoolComponentActivate, IPoolComponentDeactivate
{
    [SerializeField] private float _startForce = 1000;
    [SerializeField] private float _forcePerUpgradeLevel = 100;
    [SerializeField] private int _damadge = 50;
    [SerializeField] private int _breakingCapacityMax = 1;

    [SerializeField] private ParticleSystem _poofEffect;

    private Rigidbody _rigidbody;
    private PoolObject _poolObject;
    private Collider _collider;

    private float _bulletDestroyTime = 3f;

    private int _forceLevel;
    private int _breakingCapacity;

    private Coroutine _coroutine;
    private WaitForSeconds _despawnDelay;

    private void Awake()
    {
        _despawnDelay = new WaitForSeconds(_bulletDestroyTime);
        _rigidbody = GetComponent<Rigidbody>();
        _poolObject = GetComponent<PoolObject>();
        _collider = GetComponent<Collider>();

        ForceUpgrade forceUpgrade = new ForceUpgrade();
        ProjectilePenetrationUpgrade projectile = new ProjectilePenetrationUpgrade();

        Init(projectile.GetLevel + 1, forceUpgrade.GetLevel);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Enemy enemy))
        {
            enemy.TakeDamage(_damadge);
            enemy.EffectActivate(transform);
            _breakingCapacity++;
            if (_breakingCapacity >= _breakingCapacityMax)
            {
                _poolObject.Deactivate();
            }
        }
        else
        {
            Instantiate(_poofEffect,transform.position, Quaternion.identity);
            _poolObject.Deactivate();
        }
    }

    public void Activate()
    {
        if (_coroutine != null)
            StopCoroutine(_coroutine);
        _coroutine = StartCoroutine(ResetBulletBetweenTime());
        _collider.enabled = true;

        _rigidbody.isKinematic = false;
        _rigidbody.AddForce(transform.forward * (_startForce + _forceLevel * _forcePerUpgradeLevel));
        _breakingCapacity = 0;
    }

    public void Deactivate()
    {
        _collider.enabled = false;

        _rigidbody.isKinematic = true;
    }

    public void Init(int breaking, int forceLevel)
    {
        SetBreakingCapacity(breaking);
        SetForceLevel(forceLevel);
    }

    public void SetBreakingCapacity(int breaking)
    {
        _breakingCapacityMax = breaking;
    }

    public void SetForceLevel(int level)
    {
        _forceLevel = level;
    }

    private IEnumerator ResetBulletBetweenTime()
    {
        yield return _despawnDelay;
        _poolObject.Deactivate();
    }
}
