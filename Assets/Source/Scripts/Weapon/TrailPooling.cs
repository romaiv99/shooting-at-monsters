using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TrailRenderer))]
public class TrailPooling : MonoBehaviour, IPoolComponentActivate, IPoolComponentDeactivate
{
    private TrailRenderer _trail;

    private void Awake()
    {
        _trail = GetComponent<TrailRenderer>();
    }

    public void Activate()
    {
        _trail.enabled = true;
        _trail.Clear();
    }

    public void Deactivate()
    {
        _trail.Clear();
        _trail.enabled = false;
    }
}
