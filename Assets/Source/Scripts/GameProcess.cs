using Newtonsoft.Json.Linq;
using System;
using UnityEngine;

public class GameProcess : MonoBehaviour
{
    [SerializeField] private Player _player;

    [SerializeField] private SpawnerTimer _spawnerTimer;

    [field: SerializeField] public int MaxEnemyPerEndGame { get; private set; } = 10;

    public static bool GameRun = true;

    public event Action<int> EndingGame;

    private int EnemyKillCount => _spawnerTimer.AllSpawningEnemy - _spawnerTimer.CurrentEnemy;

    private void Awake()
    {
        GameRun = true;

        Time.timeScale = 1;

        _player.Die += EndGame;

        _spawnerTimer.EnemyCountUpdate += OnEnemyCountUpdate;
    }
    private void OnDestroy()
    {
        _player.Die -= EndGame;

        _spawnerTimer.EnemyCountUpdate -= OnEnemyCountUpdate;
    }

    private void OnEnemyCountUpdate(int value)
    {
        if (GameRun && value >= MaxEnemyPerEndGame)
        {
            EndGame();
        }
    }

    private void EndGame()
    {
        GameRun = false;
        EndingGame?.Invoke(EnemyKillCount);
    }
}
