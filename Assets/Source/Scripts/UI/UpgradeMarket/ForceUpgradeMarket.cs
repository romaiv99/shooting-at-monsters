using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceUpgradeMarket : UpgradeMarketViewBase
{
    private void Start()
    {
        Init(new ForceUpgrade());
    }
}
