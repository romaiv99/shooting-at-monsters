using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadSpeedUpgradeMarket : UpgradeMarketViewBase
{
    void Start()
    {
        Init(new ReloadSpeedUpgrade());
    }
}

