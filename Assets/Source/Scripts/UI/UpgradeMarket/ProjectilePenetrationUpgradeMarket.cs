using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePenetrationUpgradeMarket : UpgradeMarketViewBase
{
    void Start()
    {
        Init(new ProjectilePenetrationUpgrade());
    }
}

