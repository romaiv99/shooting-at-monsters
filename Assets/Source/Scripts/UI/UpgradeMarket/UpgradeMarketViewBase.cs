using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public abstract class UpgradeMarketViewBase : MonoBehaviour
{
    [SerializeField] private TMP_Text _level;
    [SerializeField] private TMP_Text _price;

    [SerializeField] private int _startPrice = 10;

    [SerializeField] private int _addPerLevelPrice = 5;

    private Button _button;

    private UpgradeBase _upgrade;

    private int GetCurrentPrice => _startPrice + _addPerLevelPrice * _upgrade.GetLevel;

    private void Awake()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(BuyUgrade);
    }

    private void OnEnable()
    {
        if (_upgrade != null)
            UpdateInfo(_upgrade.GetLevel, _upgrade.MaxLevel);
    }

    private void OnDestroy()
    {
        _button.onClick.RemoveListener(BuyUgrade);

        if (_upgrade != null)
            _upgrade.Upgraded -= UpdateInfo;
    }

    protected void Init(UpgradeBase upgrade)
    {
        _upgrade = upgrade;
        _upgrade.Upgraded += UpdateInfo;

        UpdateInfo(_upgrade.GetLevel, _upgrade.MaxLevel);
    }

    private void UpdateInfo(int value, int maxValue)
    {
        _level.text = $"{value}/{maxValue}";
        _price.text = GetCurrentPrice.ToString() + "$";

        _button.interactable = _upgrade.CanUpgeade;
    }

    private void BuyUgrade()
    {
        if (_upgrade.CanUpgeade)
        {
            if (Wallet.TryBuy(GetCurrentPrice))
            {
                _upgrade.Upgrade();
            }
        }
    }
}
