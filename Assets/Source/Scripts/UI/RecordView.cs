using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TMP_Text))]
public class RecordView : MonoBehaviour
{
    private TMP_Text _recordText;

    private void Awake()
    {
        _recordText = GetComponent<TMP_Text>();
    }

    private void OnEnable()
    {
        _recordText.text = Record.GetCurrentRecord.ToString();
    }
}
