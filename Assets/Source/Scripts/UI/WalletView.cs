using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TMP_Text))]
public class WalletView : MonoBehaviour
{
    private TMP_Text _coinCountText;

    private void Awake()
    {
        _coinCountText = GetComponent<TMP_Text>();

        Wallet.MoneyChanged += OnMoneyChanged;
        OnMoneyChanged(Wallet.GetMoney);
    }

    private void OnDestroy()
    {
        Wallet.MoneyChanged -= OnMoneyChanged;
    }

    private void OnMoneyChanged(int value)
    {
        _coinCountText.text = value.ToString()+"$";
    }
}
