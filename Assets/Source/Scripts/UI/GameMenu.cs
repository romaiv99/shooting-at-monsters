using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{
    [SerializeField] private SpawnerTimer _spawnerTimer;

    [SerializeField] private Slider _slider;
    [SerializeField] private Image _fillImage;

    [SerializeField] private GameProcess _gameProcess;

    [SerializeField] private Color _colorStart = Color.green;
    [SerializeField] private Color _colorEnd = Color.red;

    [SerializeField] private TMP_Text _text;

    [SerializeField] private GameObject _menu;

    private void Awake()
    {
        _spawnerTimer.EnemyCountUpdate += OnEnemyCountUpdate;
        _slider.maxValue = _gameProcess.MaxEnemyPerEndGame;
        _slider.value = 0;
        _text.text = "0";
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && GameProcess.GameRun)
        {
            MenuInput();
        }
    }

    private void OnDestroy()
    {
        _spawnerTimer.EnemyCountUpdate -= OnEnemyCountUpdate;
    }

    public void MenuInput()
    {
        _menu.SetActive(!_menu.activeSelf);
        Time.timeScale = _menu.activeSelf ? 0 : 1;
    }

    private void OnEnemyCountUpdate(int value)
    {
        _slider.value = value;
        _fillImage.color = Color.Lerp(_colorStart, _colorEnd, value / _slider.maxValue);
        _text.text = value.ToString();
    }
}
