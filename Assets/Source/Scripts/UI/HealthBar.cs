using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class HealthBar : MonoBehaviour
{
    [SerializeField] private Health _health;
    [SerializeField] private float _speed = 4f;

    private Slider _slider;
    private Coroutine _coroutine;

    private void Awake()
    {
        _slider = GetComponent<Slider>();
        _slider.maxValue = _health.MaxHealth;
        _slider.value = _health.CurentHealth;
    }

    private void OnEnable()
    {
        _health.HealthChange += OnHealthChange;
    }
    
    private void OnDisable()
    {
        _health.HealthChange -= OnHealthChange;
    }
    
    private void OnHealthChange(int health, int maxHealth)
    {
        _slider.maxValue = maxHealth;
        if(_coroutine!=null)
            StopCoroutine(_coroutine);
        _coroutine = StartCoroutine(SmoothSliderValueChange(health));
    }

    private IEnumerator SmoothSliderValueChange(float target)
    {
        while (_slider.value != target)
        {
            _slider.value = Mathf.MoveTowards(_slider.value, target, _speed * Time.deltaTime);
            yield return null;
        }
    }
}
