using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndGameMenu : MonoBehaviour
{
    [SerializeField] private GameProcess _gameProcess;

    [SerializeField] private GameObject _endGamePanel;

    [SerializeField] private TMP_Text _money;
    [SerializeField] private TMP_Text _enemyKill;
    [SerializeField] private TMP_Text _record;

    private void Awake()
    {
        _gameProcess.EndingGame += OnEndingGame;
    }

    private void OnDestroy()
    {
        _gameProcess.EndingGame -= OnEndingGame;
    }

    private void OnEndingGame(int value)
    {
        Debug.Log(value);

        _endGamePanel.SetActive(true);
        int addMoney = value + value / 3;
        Wallet.AddMoney(addMoney);
        _money.text = addMoney + "$";
        _enemyKill.text = value.ToString();
        _record.text = Record.TryUpdateRecord(value).ToString();

        _gameProcess.EndingGame -= OnEndingGame;
    }
}
