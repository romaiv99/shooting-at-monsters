using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class LoadingBar : MonoBehaviour
{
    [SerializeField] private SceneLoader _loader;

    private Slider _slider;

    private void Awake()
    {
        _slider = GetComponent<Slider>();

        _loader.LoadingProgress += LoadingProgress;
    }

    private void OnDestroy()
    {
        _loader.LoadingProgress -= LoadingProgress;
    }

    private void LoadingProgress(float value)
    {
        _slider.value = value;
    }
}
