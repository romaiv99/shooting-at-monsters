using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private int _sceneLoadId = 0;

    public event Action<float> LoadingProgress;

    public void CloseApplication()
    {
        Application.Quit();
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadScenes()
    {
        LoadScenes(_sceneLoadId);
    }

    public void LoadScenesAsync()
    {
        LoadScenesAsync(_sceneLoadId);
    }

    public void LoadScenes(int sceneId)
    {
        if (CheckSceneId(sceneId))
            SceneManager.LoadScene(sceneId);
    }

    public void LoadScenesAsync(int sceneId)
    {
        if (CheckSceneId(sceneId))
        {
            AsyncOperation asyncOperator = SceneManager.LoadSceneAsync(sceneId);
            StartCoroutine(Loading(asyncOperator));
        }
    }

    private bool CheckSceneId(int sceneId)
    {
        if (sceneId != SceneManager.GetActiveScene().buildIndex)
        {
            return true;
        }
        else
        {
            Debug.LogError("Load scene ID == Active Scene ID");
            return false;
        }
    }

    private IEnumerator Loading(AsyncOperation asyncOperator)
    {
        while (!asyncOperator.isDone)
        {
            LoadingProgress?.Invoke(asyncOperator.progress);
            yield return null;
        }
    }
}
