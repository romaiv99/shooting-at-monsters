using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadSceneScript : MonoBehaviour
{
    [SerializeField] private SceneLoader _sceneLoader;

    private void Start()
    {
        _sceneLoader.LoadScenesAsync(1);
    }
}
