using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpawnerTimer : MonoBehaviour
{
    [SerializeField] private EnemySpawner[] _spawners;

    [SerializeField] private float _startSpawnTime = 2f;
    [SerializeField] private float _minSpawnTime = 0.3f;
    [SerializeField] private float _spawnTimeDecrease = 0.2f;
    [SerializeField] private int _spawnCountToLevelSpawnUp = 5;

    private List<Enemy> _enemies = new List<Enemy>();

    public int AllSpawningEnemy { get; private set; }

    public int CurrentEnemy => _enemies.Count;

    public event Action<int> EnemyCountUpdate;

    private void Start()
    {
        StartCoroutine(EndlessSpawn());
    }

    private IEnumerator EndlessSpawn()
    {
        while (GameProcess.GameRun)
        {
            int randomSpawner = Random.Range(0, _spawners.Length);

            _enemies.Add(_spawners[randomSpawner].Spawn());
            _enemies[_enemies.Count-1].DieInfo += OnDieInfo;
            EnemyCountUpdate?.Invoke(_enemies.Count);
            AllSpawningEnemy++;
            yield return new WaitForSeconds(Mathf.Max(
                    _startSpawnTime - AllSpawningEnemy / _spawnCountToLevelSpawnUp * _spawnTimeDecrease, 
                    _minSpawnTime));
        }
    }

    private void OnDieInfo(Enemy enemy)
    {
        enemy.DieInfo -= OnDieInfo;

        _enemies.Remove(enemy);

        EnemyCountUpdate?.Invoke(_enemies.Count);
    }
}
