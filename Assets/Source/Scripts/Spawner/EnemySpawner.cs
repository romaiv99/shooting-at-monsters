using System.Collections;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Pool _pool;
    [SerializeField] private Transform _spawnPoint;
    [SerializeField] private Transform _target;

    [SerializeField] private float _randomRadius = 2f;

    public Enemy Spawn()
    {
        return PositionSpawnObject();
    }

    private Enemy PositionSpawnObject()
    {
        PoolObject objectPool = _pool.GetObject();
        objectPool.transform.position = _spawnPoint.position
            + new Vector3(Random.Range(-_randomRadius, _randomRadius), 0, 0)
            + new Vector3(0, 0, Random.Range(-_randomRadius, _randomRadius));
        objectPool.transform.rotation = _spawnPoint.rotation;
        objectPool.Activate();
        objectPool.GetComponent<EnemyMover>().SetTarget(_target);
        return objectPool.GetComponent<Enemy>();
    }
}
