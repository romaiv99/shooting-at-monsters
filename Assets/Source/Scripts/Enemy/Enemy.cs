using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider), typeof(PoolObject))]
public class Enemy : Health, IPoolComponentActivate, IPoolComponentDeactivate
{
    private const int MaxHealthInit = 100;

    [SerializeField] private ParticleSystem _blood;

    private EnemyAnimation _enemyAnimation;
    private Collider _collider;
    private PoolObject _poolObject;

    private float _returnToPoolTime = 3;

    public event Action<Enemy> DieInfo;

    private void Awake()
    {
        _enemyAnimation = GetComponentInChildren<EnemyAnimation>();
        _collider = GetComponent<Collider>();
        _poolObject = GetComponent<PoolObject>();
    }

    public void EffectActivate(Transform transformHit)
    {
        _blood.transform.position = transformHit.position;
        _blood.transform.LookAt(transformHit);
        _blood.Play();
    }

    protected override void Died()
    {
        base.Died();
        DieInfo?.Invoke(this);
        _enemyAnimation.Die(true);
        _collider.enabled = false;
        StartCoroutine(ReturnToPool());
    }

    public void Rewiwe()
    {
        _enemyAnimation.Die(false);
        _collider.enabled = true;
    }

    public void Activate()
    {
        Rewiwe();
        Init(MaxHealthInit, MaxHealthInit);
    }

    public void Deactivate()
    {
        _collider.enabled = false;
    }

    private IEnumerator ReturnToPool()
    {
        yield return new WaitForSeconds(_returnToPoolTime);
        _poolObject.Deactivate();
    }
}
