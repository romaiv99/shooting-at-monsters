using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class EnemyAnimation : MonoBehaviour
{
    private Animator _animator;

    private readonly int AttackHash = Animator.StringToHash("Attack");
    private readonly int SpeedHash = Animator.StringToHash("Speed");
    private readonly int DieHash = Animator.StringToHash("Die");

    private const float DempTime = 0.1f;
    
    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    public void Attack()
    {
        _animator.SetTrigger(AttackHash);
    }

    public void Die(bool isDie)
    {
        _animator.SetBool(DieHash, isDie);
    }

    public void SetRunSpeed(float speed)
    {
        _animator.SetFloat(SpeedHash,speed,DempTime,Time.deltaTime);
    }
}
