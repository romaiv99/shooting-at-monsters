using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSkin : MonoBehaviour, IPoolComponentActivate
{
    [SerializeField] private GameObject[] _skinWariant;

    public void Activate()
    {
        SetRandomSkin();
    }

    public void SetRandomSkin()
    {
        foreach (GameObject skin in _skinWariant)
        {
            skin.SetActive(false);
        }
        
        _skinWariant[Random.Range(0,_skinWariant.Length)].SetActive(true);
    }
}
