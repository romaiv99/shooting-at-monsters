using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent), typeof(Enemy))]
public class EnemyMover : MonoBehaviour, IPoolComponentActivate, IPoolComponentDeactivate
{
    [SerializeField] private Transform _target;
    [SerializeField] private Transform _attackCenterPoint;
    [SerializeField] private float _attackRadius = 1;

    [SerializeField] private float _attackDistance = 1;
    [SerializeField] private float _attackDuraction = 2;
    [SerializeField] private int _damadge = 10;

    [SerializeField] private bool _renderEditorAttackSphear = true;

    private NavMeshAgent _navMeshAgent;
    private EnemyAnimation _enemyAnimation;

    private Enemy _enemy;

    private bool _isAttackAvalible = true;

    private Coroutine _attackWaiter;

    private void Awake()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _enemy = GetComponent<Enemy>();

        _enemyAnimation = GetComponentInChildren<EnemyAnimation>();

        _enemy.Die += EnemyOnDie;

        _navMeshAgent.stoppingDistance = _attackDistance;

        _navMeshAgent.updateRotation = false;
    }

    private void Update()
    {
        if (_target != null && !_enemy.IsDead)
        {
            Vector3 diff = _target.position - transform.position;

            float rot = Mathf.Atan2(diff.x, diff.z) * Mathf.Rad2Deg;

            transform.rotation = Quaternion.RotateTowards(transform.rotation,
                Quaternion.Euler(0, rot, 0), Time.deltaTime * _navMeshAgent.angularSpeed);

            if (Vector3.Distance(_target.position, transform.position) < _attackDistance)
            {
                if (_isAttackAvalible)
                {
                    _enemyAnimation.Attack();
                    _attackWaiter = StartCoroutine(AttackCoruntine());
                }
            }
            else if (_isAttackAvalible)
            {
                _navMeshAgent.SetDestination(_target.position);
            }

            _enemyAnimation.SetRunSpeed(_navMeshAgent.velocity.magnitude);
        }
    }

    private void OnDestroy()
    {
        _enemy.Die -= EnemyOnDie;
    }

    private void OnDrawGizmos()
    {
        if (_renderEditorAttackSphear)
            Gizmos.DrawSphere(_attackCenterPoint.position, _attackRadius);
    }

    public void SetTarget(Transform target)
    {
        _target = target;
    }

    public void Activate()
    {
        _navMeshAgent.enabled = true;
        _isAttackAvalible = true;
    }

    public void Deactivate()
    {
        EnemyOnDie();
    }

    private void EnemyOnDie()
    {
        _navMeshAgent.enabled = false;

        if (_attackWaiter != null)
        {
            StopCoroutine(_attackWaiter);
        }
    }

    private IEnumerator AttackCoruntine()
    {
        _isAttackAvalible = false;
        yield return new WaitForSeconds(_attackDuraction / 2);

        Collider[] colliders = Physics.OverlapSphere(_attackCenterPoint.position, _attackRadius);

        foreach (var collider in colliders)
        {
            if (collider.TryGetComponent(out Player player))
                player.TakeDamage(_damadge);
        }

        yield return new WaitForSeconds(_attackDuraction / 2);
        _isAttackAvalible = true;
    }
}
