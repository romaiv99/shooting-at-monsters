using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Health
{
    [SerializeField] private Transform _shootPoint;
    [SerializeField] private Weapon _weapon;

    [SerializeField] private float _freeDistanceWorldPoint = 15;

    private void Update()
    {
        if (!IsDead && GameProcess.GameRun)
        {
            if (Input.GetMouseButton(0))
            {
                if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit raycastHit,
                    _freeDistanceWorldPoint))
                {
                    _shootPoint.LookAt(raycastHit.point);
                }
                else
                {
                    Vector3 inputPosition = new Vector3(Input.mousePosition.x, 
                        Input.mousePosition.y, _freeDistanceWorldPoint);

                    Vector3 inputWorldPosition = Camera.main.ScreenToWorldPoint(inputPosition);
                    _shootPoint.LookAt(inputWorldPosition);
                }
                
                _weapon.Fire();
            }
        }
    }

    protected override void Died()
    {
        base.Died();
    }
}
