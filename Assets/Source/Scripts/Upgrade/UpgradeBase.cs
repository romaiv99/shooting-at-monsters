using System;
using UnityEngine;

public abstract class UpgradeBase
{
    protected Upgrades _upgrade;

    protected int _maxLevel;

    public event Action<int, int> Upgraded;

    public UpgradeBase(Upgrades upgrades, int maxLevel)
    {
        _upgrade = upgrades;
        _maxLevel = maxLevel;
    }

    public string UpgradeEnumName => _upgrade.ToString();
    public int MaxLevel => _maxLevel;

    public int GetLevel => PlayerPrefs.GetInt(UpgradeEnumName);

    public bool CanUpgeade => GetLevel < _maxLevel;

    public void Upgrade()
    {
        if (CanUpgeade)
        {
            int level = GetLevel;
            PlayerPrefs.SetInt(UpgradeEnumName, level + 1);
            Upgraded?.Invoke(level + 1, _maxLevel);
        }
    }
}
