using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadSpeedUpgrade : UpgradeBase
{
    private const int MaxUpgradeLevel = 5;

    public ReloadSpeedUpgrade() : base(Upgrades.ReloadSpeed, MaxUpgradeLevel)
    {
    }
}
