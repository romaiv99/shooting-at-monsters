using UnityEngine;

public class ForceUpgrade : UpgradeBase
{
    private const int MaxUpgradeLevel = 25;

    public ForceUpgrade() : base(Upgrades.Force, MaxUpgradeLevel)
    {
    }
}
