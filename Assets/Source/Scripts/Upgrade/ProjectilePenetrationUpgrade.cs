using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePenetrationUpgrade : UpgradeBase
{
    private const int MaxUpgradeLevel = 5;

    public ProjectilePenetrationUpgrade() : base(Upgrades.ProjectilePenetration, MaxUpgradeLevel)
    {
    }
}
