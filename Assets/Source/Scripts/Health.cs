using System;
using UnityEngine;

public class Health : MonoBehaviour
{
    [field: SerializeField] public int MaxHealth { get; private set; } = 100;

    [field: SerializeField] public int CurentHealth { get; private set; } = 100;

    public event Action<int, int> HealthChange;
    public event Action Die;

    public bool IsDead => CurentHealth <= 0;

    private bool _dieFirst = false;

    public void Init(int health, int maxHealth)
    {
        MaxHealth = maxHealth;
        SetHealth(health);

        _dieFirst = false;
    }

    public void TakeDamage(int damage)
    {
        if (damage > 0)
        {
            SetHealth(CurentHealth - damage);
        }
    }

    public void Heal(int heal)
    {
        if (heal > 0)
        {
            SetHealth(CurentHealth + heal);
        }
    }

    protected virtual void Died() 
    {
        if (!_dieFirst)
        {
            Die?.Invoke();
            _dieFirst = true;
        }
    }

    private void SetHealth(int hp)
    {
        CurentHealth = Mathf.Clamp(hp, 0, MaxHealth);
        HealthChange?.Invoke(CurentHealth, MaxHealth);

        if (IsDead)
        {            
            Died();
        }
    }
}
