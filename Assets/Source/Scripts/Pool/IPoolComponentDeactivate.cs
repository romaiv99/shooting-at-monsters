using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPoolComponentDeactivate
{
    public void Deactivate();
}
