using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class PoolObject : MonoBehaviour
{
    public bool IsActive { get; private set; } = true;

    private Pool _pool;

    private IPoolComponentActivate[] _poolComponentsActivate;
    private IPoolComponentDeactivate[] _poolComponentsDeactivate;

    public void Init(Pool pool)
    {
        _pool = pool;

        _poolComponentsActivate = GetComponentsInChildren<IPoolComponentActivate>();
        _poolComponentsDeactivate = GetComponentsInChildren<IPoolComponentDeactivate>();
    }
    
    public virtual void Activate()
    {
        IsActive = true;

        foreach (IPoolComponentActivate poolComponent in _poolComponentsActivate)
        {
            poolComponent.Activate();
        }
    }
    
    public virtual void Deactivate()
    {       
        foreach (IPoolComponentDeactivate poolComponent in _poolComponentsDeactivate)
        {
            poolComponent.Deactivate();
        }
        IsActive = false;
        ReturnInPool();
    }

    private void ReturnInPool()
    {
        _pool.ReturnInPool(this);
    }
}
