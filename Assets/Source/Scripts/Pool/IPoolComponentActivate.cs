using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPoolComponentActivate
{
    public void Activate();   
}
