using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pool : MonoBehaviour
{
    [SerializeField] private PoolObject _template;
    [SerializeField] private int _startCount = 10;
    [SerializeField] private Transform _storagePoint;

    private List<PoolObject> _poolList;

    private void Awake()
    {
        _poolList = new List<PoolObject>(_startCount);

        for (int i = 0; i < _startCount; i++)
        {
            CreateObjectToPool().Deactivate();
        }
    }

    public void ReturnInPool(PoolObject objectPool)
    {
        objectPool.transform.position = _storagePoint.position;
    }

    public PoolObject GetObject()
    {
        PoolObject returnedObject = _poolList.FirstOrDefault(_ => !_.IsActive);

        if (returnedObject == null)
            return CreateObjectToPool();

        return returnedObject;
    }
    
    private PoolObject CreateObjectToPool()
    {
        PoolObject poolObject = Instantiate(_template, _storagePoint.position, Quaternion.identity);
        _poolList.Add(poolObject);
        poolObject.Init(this);
        return poolObject;
    }
}
