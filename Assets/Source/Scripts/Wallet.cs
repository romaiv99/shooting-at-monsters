using System;
using UnityEngine;

public static class Wallet
{
    private const string MoneySaveKey = "Money";

    public static int GetMoney => PlayerPrefs.GetInt(MoneySaveKey);

    public static event Action<int> MoneyChanged;

    public static void AddMoney(int amount)
    {
        if (amount > 0)
        {
            ChangeMoney(amount);
        }
    }

    public static bool TryBuy(int amount)
    {
        if (GetMoney - amount >= 0)
        {
            ChangeMoney(-amount);
            return true;
        }
        return false;
    }

    private static void ChangeMoney(int amount)
    {
        PlayerPrefs.SetInt(MoneySaveKey, GetMoney + amount);
        MoneyChanged?.Invoke(GetMoney);
    }
}
